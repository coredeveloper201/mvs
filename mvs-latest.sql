-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 04, 2019 at 07:09 AM
-- Server version: 10.1.38-MariaDB
-- PHP Version: 7.2.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `mvs`
--

-- --------------------------------------------------------

--
-- Table structure for table `angebote`
--

CREATE TABLE `angebote` (
  `id` int(10) NOT NULL,
  `teildarlehen` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `darlehensbetrag` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sollzinsbindung` int(11) DEFAULT NULL,
  `sollzinssatz` int(11) DEFAULT NULL,
  `eff_jahreszins_pangv` int(11) DEFAULT NULL,
  `kaufpreis` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `kostenumbau` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `kostennotar` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `grunderwerbssteuer` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `maklerkosten` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gesamtkosten` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `eigenkapital` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `finanzierungsbedarf` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pdf_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `customer_id` int(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `angebote`
--

INSERT INTO `angebote` (`id`, `teildarlehen`, `darlehensbetrag`, `sollzinsbindung`, `sollzinssatz`, `eff_jahreszins_pangv`, `kaufpreis`, `kostenumbau`, `kostennotar`, `grunderwerbssteuer`, `maklerkosten`, `gesamtkosten`, `eigenkapital`, `finanzierungsbedarf`, `pdf_name`, `customer_id`) VALUES
(277, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1.pdf', 1),
(278, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '278.pdf', 1),
(279, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '279.pdf', 1),
(280, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '280.pdf', 1),
(281, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '281.pdf', 1),
(282, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '282.pdf', 1),
(283, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '283.pdf', 1),
(284, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '284.pdf', 1),
(285, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '285.pdf', 1),
(286, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '286.pdf', 1),
(287, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '287.pdf', 1),
(288, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '288.pdf', 1),
(289, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '289.pdf', 1),
(290, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '290.pdf', 1),
(291, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '291.pdf', 1),
(292, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '292.pdf', 1),
(293, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '293.pdf', 1),
(294, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '294.pdf', 1),
(295, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '295.pdf', 1),
(296, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '296.pdf', 1),
(297, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '297.pdf', 1),
(298, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '298.pdf', 1),
(299, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '299.pdf', 1),
(300, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '300.pdf', 1),
(301, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '301.pdf', 1),
(302, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '302.pdf', 1),
(303, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '303.pdf', 1),
(304, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '304.pdf', 1),
(305, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '305.pdf', 1),
(306, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '306.pdf', 1),
(307, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '307.pdf', 1),
(308, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '308.pdf', 1),
(309, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '309.pdf', 1),
(310, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '310.pdf', 1),
(311, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '311.pdf', 1),
(312, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '312.pdf', 1),
(313, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '313.pdf', 1),
(314, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '314.pdf', 1),
(315, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '315.pdf', 1),
(316, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '316.pdf', 1),
(317, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '317.pdf', 1),
(318, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '318.pdf', 1),
(319, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '319.pdf', 1),
(320, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '320.pdf', 1),
(321, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '321.pdf', 1),
(322, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '322.pdf', 1),
(323, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '323.pdf', 1),
(324, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '324.pdf', 1),
(325, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '325.pdf', 1),
(326, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '326.pdf', 1),
(327, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '327.pdf', 1),
(328, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '328.pdf', 1),
(329, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '329.pdf', 1),
(330, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '330.pdf', 1),
(331, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '331.pdf', 1),
(332, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '332.pdf', 1),
(333, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '333.pdf', 1),
(334, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '334.pdf', 1),
(335, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '335.pdf', 1),
(336, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '336.pdf', 1),
(337, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '337.pdf', 1),
(338, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '338.pdf', 1),
(339, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '339.pdf', 1),
(340, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '340.pdf', 1),
(341, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '341.pdf', 1),
(342, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '342.pdf', 1),
(343, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '343.pdf', 1),
(344, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '344.pdf', 1),
(345, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '345.pdf', 1),
(346, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '346.pdf', 1),
(347, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '347.pdf', 1),
(348, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '348.pdf', 1),
(349, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '349.pdf', 1),
(350, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '350.pdf', 8),
(351, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '351.pdf', 1);

-- --------------------------------------------------------

--
-- Table structure for table `calculation`
--

CREATE TABLE `calculation` (
  `id` int(11) NOT NULL,
  `kunden_id` int(191) NOT NULL,
  `prepared_by` int(11) NOT NULL,
  `bank` varchar(255) NOT NULL,
  `annuities` varchar(255) NOT NULL,
  `to_interest` varchar(255) NOT NULL,
  `effectiveness` varchar(255) NOT NULL,
  `fixed_interest_rates` varchar(255) NOT NULL,
  `monthly_loan` varchar(255) NOT NULL,
  `residual_debt_interest_rate` varchar(255) NOT NULL,
  `calculated_luaf_time` varchar(255) NOT NULL,
  `net_loan_amount` varchar(255) NOT NULL,
  `initial_interest` varchar(255) NOT NULL,
  `optional_sound_recovery` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `calculation`
--

INSERT INTO `calculation` (`id`, `kunden_id`, `prepared_by`, `bank`, `annuities`, `to_interest`, `effectiveness`, `fixed_interest_rates`, `monthly_loan`, `residual_debt_interest_rate`, `calculated_luaf_time`, `net_loan_amount`, `initial_interest`, `optional_sound_recovery`, `created_at`, `updated_at`) VALUES
(1, 1, 5, 'Deutsche Bank', 'Annuitäten', '1', '2', '3', '4', '5', '6', '7', '8', '9', '2019-04-03 12:01:07', '2019-04-04 05:07:27'),
(2, 1, 5, 'Deutsche Bank', 'Annuitäten', '22', '22', '22', '22', '22', '22', '22', '22', '22', '2019-04-04 04:51:06', '2019-04-04 05:07:27');

-- --------------------------------------------------------

--
-- Table structure for table `groups`
--

CREATE TABLE `groups` (
  `id` int(3) NOT NULL,
  `name` varchar(50) DEFAULT NULL,
  `address` varchar(50) DEFAULT NULL,
  `postal_code` int(11) DEFAULT NULL,
  `city` varchar(50) DEFAULT NULL,
  `street` varchar(50) DEFAULT NULL,
  `street_num` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `groups`
--

INSERT INTO `groups` (`id`, `name`, `address`, `postal_code`, `city`, `street`, `street_num`, `created_at`, `updated_at`) VALUES
(1, 'Nettetal - Kintzelmann', 'dssdf', 47906, 'Nordrhein-Westfalen - Kempen', 'Görtschesweg', 4, '2019-02-26 06:40:18', '2019-02-26 06:40:18'),
(2, 'Hückelhoven - Sternad', NULL, 12345, 'Hückelhoven', 'Hückelstraße', 3, '2019-02-26 06:50:21', '2019-02-26 06:50:21');

-- --------------------------------------------------------

--
-- Table structure for table `immobilies`
--

CREATE TABLE `immobilies` (
  `id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `kaufpreis` int(11) NOT NULL,
  `kosten_umbau_modernisierung` int(11) NOT NULL,
  `kosten_notar_grundbuch` int(11) NOT NULL,
  `grunderwerbssteuer` int(11) NOT NULL,
  `maklerkosten` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `kundens`
--

CREATE TABLE `kundens` (
  `id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `vorname` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nachname` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `strasse` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `plz` int(11) NOT NULL,
  `wohnort` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mail` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `telefon` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `geburtsdatum` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `kaufpreis` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `kostenumbau` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `kostennotar` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `grunderwerbssteuer` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `maklerkosten` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gesamtkosten` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `eigenkapital` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `finanzierungsbedarf` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `kundens`
--

INSERT INTO `kundens` (`id`, `created_at`, `updated_at`, `user_id`, `vorname`, `nachname`, `strasse`, `plz`, `wohnort`, `mail`, `telefon`, `geburtsdatum`, `kaufpreis`, `kostenumbau`, `kostennotar`, `grunderwerbssteuer`, `maklerkosten`, `gesamtkosten`, `eigenkapital`, `finanzierungsbedarf`) VALUES
(1, '2019-02-26 06:40:55', '2019-04-03 06:38:11', 27, 'Patrickk', 'Dierig', 'Görtschesweg, 4', 47906, 'Nordrhein-Westfalen - Kempen', 'p-dierig@einfachmedial.de', '015204967292', '2017-10-30', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2018_11_21_101915_create_tests_table', 1),
(2, '2018_11_21_130836_create_timeline', 2),
(3, '2018_11_21_134854_update_zeitstrahl_tabelle', 3),
(4, '2018_11_21_161940_update_zeitstrahl_tabelle_zwei', 4);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `repayments`
--

CREATE TABLE `repayments` (
  `id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `zinsen` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tilgung` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `darlehensrest` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `kundens_id` int(11) NOT NULL,
  `repayment_date` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `repayments`
--

INSERT INTO `repayments` (`id`, `created_at`, `updated_at`, `zinsen`, `tilgung`, `darlehensrest`, `kundens_id`, `repayment_date`) VALUES
(241, '2019-04-03 04:15:08', '2019-04-03 04:15:08', 'NaN €', 'NaN €', 'NaN €', 1, 'Mai 2018'),
(242, '2019-04-03 04:15:08', '2019-04-03 04:15:08', 'NaN €', 'NaN €', 'NaN €', 1, 'Jun 2018'),
(243, '2019-04-03 04:15:08', '2019-04-03 04:15:08', 'NaN €', 'NaN €', 'NaN €', 1, 'Jul 2018'),
(244, '2019-04-03 04:15:08', '2019-04-03 04:15:08', 'NaN €', 'NaN €', 'NaN €', 1, 'Aug 2018'),
(245, '2019-04-03 04:15:08', '2019-04-03 04:15:08', 'NaN €', 'NaN €', 'NaN €', 1, 'Sep 2018'),
(246, '2019-04-03 04:15:08', '2019-04-03 04:15:08', 'NaN €', 'NaN €', 'NaN €', 1, 'Okt 2018'),
(247, '2019-04-03 04:15:08', '2019-04-03 04:15:08', 'NaN €', 'NaN €', 'NaN €', 1, 'Nov 2018'),
(248, '2019-04-03 04:15:08', '2019-04-03 04:15:08', 'NaN €', 'NaN €', 'NaN €', 1, 'Dez 2018'),
(249, '2019-04-03 04:15:08', '2019-04-03 04:15:08', 'NaN €', 'NaN €', 'NaN €', 1, 'Jan 2019'),
(250, '2019-04-03 04:15:08', '2019-04-03 04:15:08', 'NaN €', 'NaN €', 'NaN €', 1, 'Mär 2019'),
(251, '2019-04-03 04:15:08', '2019-04-03 04:15:08', 'NaN €', 'NaN €', 'NaN €', 1, 'Apr 2019'),
(252, '2019-04-03 04:15:08', '2019-04-03 04:15:08', 'NaN €', 'NaN €', 'NaN €', 1, 'Mai 2019'),
(253, '2019-04-03 04:15:08', '2019-04-03 04:15:08', 'NaN €', 'NaN €', 'NaN €', 1, 'Jun 2019'),
(254, '2019-04-03 04:15:08', '2019-04-03 04:15:08', 'NaN €', 'NaN €', 'NaN €', 1, 'Jul 2019'),
(255, '2019-04-03 04:15:08', '2019-04-03 04:15:08', 'NaN €', 'NaN €', 'NaN €', 1, 'Aug 2019'),
(256, '2019-04-03 04:15:08', '2019-04-03 04:15:08', 'NaN €', 'NaN €', 'NaN €', 1, 'Sep 2019'),
(257, '2019-04-03 04:15:08', '2019-04-03 04:15:08', 'NaN €', 'NaN €', 'NaN €', 1, 'Okt 2019'),
(258, '2019-04-03 04:15:08', '2019-04-03 04:15:08', 'NaN €', 'NaN €', 'NaN €', 1, 'Nov 2019'),
(259, '2019-04-03 04:15:08', '2019-04-03 04:15:08', 'NaN €', 'NaN €', 'NaN €', 1, 'Dez 2019'),
(260, '2019-04-03 04:15:08', '2019-04-03 04:15:08', 'NaN €', 'NaN €', 'NaN €', 1, 'Jan 2020'),
(261, '2019-04-03 04:15:08', '2019-04-03 04:15:08', 'NaN €', 'NaN €', 'NaN €', 1, 'Feb 2020'),
(262, '2019-04-03 04:15:08', '2019-04-03 04:15:08', 'NaN €', 'NaN €', 'NaN €', 1, 'Mär 2020'),
(263, '2019-04-03 04:15:08', '2019-04-03 04:15:08', 'NaN €', 'NaN €', 'NaN €', 1, 'Apr 2020'),
(264, '2019-04-03 04:15:08', '2019-04-03 04:15:08', 'NaN €', 'NaN €', 'NaN €', 1, 'Mai 2020'),
(265, '2019-04-03 04:15:08', '2019-04-03 04:15:08', 'NaN €', 'NaN €', 'NaN €', 1, 'Jun 2020'),
(266, '2019-04-03 04:15:08', '2019-04-03 04:15:08', 'NaN €', 'NaN €', 'NaN €', 1, 'Jul 2020'),
(267, '2019-04-03 04:15:08', '2019-04-03 04:15:08', 'NaN €', 'NaN €', 'NaN €', 1, 'Aug 2020'),
(268, '2019-04-03 04:15:08', '2019-04-03 04:15:08', 'NaN €', 'NaN €', 'NaN €', 1, 'Sep 2020'),
(269, '2019-04-03 04:15:08', '2019-04-03 04:15:08', 'NaN €', 'NaN €', 'NaN €', 1, 'Okt 2020'),
(270, '2019-04-03 04:15:08', '2019-04-03 04:15:08', 'NaN €', 'NaN €', 'NaN €', 1, 'Nov 2020'),
(271, '2019-04-03 04:15:08', '2019-04-03 04:15:08', 'NaN €', 'NaN €', 'NaN €', 1, 'Dez 2020'),
(272, '2019-04-03 04:15:08', '2019-04-03 04:15:08', 'NaN €', 'NaN €', 'NaN €', 1, 'Jan 2021'),
(273, '2019-04-03 04:15:08', '2019-04-03 04:15:08', 'NaN €', 'NaN €', 'NaN €', 1, 'Feb 2021'),
(274, '2019-04-03 04:15:08', '2019-04-03 04:15:08', 'NaN €', 'NaN €', 'NaN €', 1, 'Mär 2021'),
(275, '2019-04-03 04:15:08', '2019-04-03 04:15:08', 'NaN €', 'NaN €', 'NaN €', 1, 'Apr 2021'),
(276, '2019-04-03 04:15:08', '2019-04-03 04:15:08', 'NaN €', 'NaN €', 'NaN €', 1, 'Mai 2021'),
(277, '2019-04-03 04:15:08', '2019-04-03 04:15:08', 'NaN €', 'NaN €', 'NaN €', 1, 'Jun 2021'),
(278, '2019-04-03 04:15:09', '2019-04-03 04:15:09', 'NaN €', 'NaN €', 'NaN €', 1, 'Jul 2021'),
(279, '2019-04-03 04:15:09', '2019-04-03 04:15:09', 'NaN €', 'NaN €', 'NaN €', 1, 'Aug 2021'),
(280, '2019-04-03 04:15:09', '2019-04-03 04:15:09', 'NaN €', 'NaN €', 'NaN €', 1, 'Sep 2021'),
(281, '2019-04-03 04:15:09', '2019-04-03 04:15:09', 'NaN €', 'NaN €', 'NaN €', 1, 'Okt 2021'),
(282, '2019-04-03 04:15:09', '2019-04-03 04:15:09', 'NaN €', 'NaN €', 'NaN €', 1, 'Nov 2021'),
(283, '2019-04-03 04:15:09', '2019-04-03 04:15:09', 'NaN €', 'NaN €', 'NaN €', 1, 'Dez 2021'),
(284, '2019-04-03 04:15:09', '2019-04-03 04:15:09', 'NaN €', 'NaN €', 'NaN €', 1, 'Jan 2022'),
(285, '2019-04-03 04:15:09', '2019-04-03 04:15:09', 'NaN €', 'NaN €', 'NaN €', 1, 'Feb 2022'),
(286, '2019-04-03 04:15:09', '2019-04-03 04:15:09', 'NaN €', 'NaN €', 'NaN €', 1, 'Mär 2022'),
(287, '2019-04-03 04:15:09', '2019-04-03 04:15:09', 'NaN €', 'NaN €', 'NaN €', 1, 'Apr 2022'),
(288, '2019-04-03 04:15:09', '2019-04-03 04:15:09', 'NaN €', 'NaN €', 'NaN €', 1, 'Mai 2022'),
(289, '2019-04-03 04:15:09', '2019-04-03 04:15:09', 'NaN €', 'NaN €', 'NaN €', 1, 'Jun 2022'),
(290, '2019-04-03 04:15:09', '2019-04-03 04:15:09', 'NaN €', 'NaN €', 'NaN €', 1, 'Jul 2022'),
(291, '2019-04-03 04:15:09', '2019-04-03 04:15:09', 'NaN €', 'NaN €', 'NaN €', 1, 'Aug 2022'),
(292, '2019-04-03 04:15:09', '2019-04-03 04:15:09', 'NaN €', 'NaN €', 'NaN €', 1, 'Sep 2022'),
(293, '2019-04-03 04:15:09', '2019-04-03 04:15:09', 'NaN €', 'NaN €', 'NaN €', 1, 'Okt 2022'),
(294, '2019-04-03 04:15:09', '2019-04-03 04:15:09', 'NaN €', 'NaN €', 'NaN €', 1, 'Nov 2022'),
(295, '2019-04-03 04:15:09', '2019-04-03 04:15:09', 'NaN €', 'NaN €', 'NaN €', 1, 'Dez 2022'),
(296, '2019-04-03 04:15:09', '2019-04-03 04:15:09', 'NaN €', 'NaN €', 'NaN €', 1, 'Jan 2023'),
(297, '2019-04-03 04:15:09', '2019-04-03 04:15:09', 'NaN €', 'NaN €', 'NaN €', 1, 'Feb 2023'),
(298, '2019-04-03 04:15:09', '2019-04-03 04:15:09', 'NaN €', 'NaN €', 'NaN €', 1, 'Mär 2023'),
(299, '2019-04-03 04:15:09', '2019-04-03 04:15:09', 'NaN €', 'NaN €', 'NaN €', 1, 'Apr 2023'),
(300, '2019-04-03 04:15:09', '2019-04-03 04:15:09', 'NaN €', 'NaN €', 'NaN €', 1, 'Mai 2023'),
(301, '2019-04-03 04:15:09', '2019-04-03 04:15:09', 'NaN €', 'NaN €', 'NaN €', 1, 'Jun 2023'),
(302, '2019-04-03 04:15:09', '2019-04-03 04:15:09', 'NaN €', 'NaN €', 'NaN €', 1, 'Jul 2023'),
(303, '2019-04-03 04:15:09', '2019-04-03 04:15:09', 'NaN €', 'NaN €', 'NaN €', 1, 'Aug 2023'),
(304, '2019-04-03 04:15:09', '2019-04-03 04:15:09', 'NaN €', 'NaN €', 'NaN €', 1, 'Sep 2023'),
(305, '2019-04-03 04:15:09', '2019-04-03 04:15:09', 'NaN €', 'NaN €', 'NaN €', 1, 'Okt 2023'),
(306, '2019-04-03 04:15:09', '2019-04-03 04:15:09', 'NaN €', 'NaN €', 'NaN €', 1, 'Nov 2023'),
(307, '2019-04-03 04:15:09', '2019-04-03 04:15:09', 'NaN €', 'NaN €', 'NaN €', 1, 'Dez 2023'),
(308, '2019-04-03 04:15:09', '2019-04-03 04:15:09', 'NaN €', 'NaN €', 'NaN €', 1, 'Jan 2024'),
(309, '2019-04-03 04:15:09', '2019-04-03 04:15:09', 'NaN €', 'NaN €', 'NaN €', 1, 'Feb 2024'),
(310, '2019-04-03 04:15:09', '2019-04-03 04:15:09', 'NaN €', 'NaN €', 'NaN €', 1, 'Mär 2024'),
(311, '2019-04-03 04:15:09', '2019-04-03 04:15:09', 'NaN €', 'NaN €', 'NaN €', 1, 'Apr 2024'),
(312, '2019-04-03 04:15:09', '2019-04-03 04:15:09', 'NaN €', 'NaN €', 'NaN €', 1, 'Mai 2024'),
(313, '2019-04-03 04:15:09', '2019-04-03 04:15:09', 'NaN €', 'NaN €', 'NaN €', 1, 'Jun 2024'),
(314, '2019-04-03 04:15:09', '2019-04-03 04:15:09', 'NaN €', 'NaN €', 'NaN €', 1, 'Jul 2024'),
(315, '2019-04-03 04:15:09', '2019-04-03 04:15:09', 'NaN €', 'NaN €', 'NaN €', 1, 'Aug 2024'),
(316, '2019-04-03 04:15:09', '2019-04-03 04:15:09', 'NaN €', 'NaN €', 'NaN €', 1, 'Sep 2024'),
(317, '2019-04-03 04:15:09', '2019-04-03 04:15:09', 'NaN €', 'NaN €', 'NaN €', 1, 'Okt 2024'),
(318, '2019-04-03 04:15:09', '2019-04-03 04:15:09', 'NaN €', 'NaN €', 'NaN €', 1, 'Nov 2024'),
(319, '2019-04-03 04:15:09', '2019-04-03 04:15:09', 'NaN €', 'NaN €', 'NaN €', 1, 'Dez 2024'),
(320, '2019-04-03 04:15:09', '2019-04-03 04:15:09', 'NaN €', 'NaN €', 'NaN €', 1, 'Jan 2025'),
(321, '2019-04-03 04:15:09', '2019-04-03 04:15:09', 'NaN €', 'NaN €', 'NaN €', 1, 'Feb 2025'),
(322, '2019-04-03 04:15:09', '2019-04-03 04:15:09', 'NaN €', 'NaN €', 'NaN €', 1, 'Mär 2025'),
(323, '2019-04-03 04:15:09', '2019-04-03 04:15:09', 'NaN €', 'NaN €', 'NaN €', 1, 'Apr 2025'),
(324, '2019-04-03 04:15:09', '2019-04-03 04:15:09', 'NaN €', 'NaN €', 'NaN €', 1, 'Mai 2025'),
(325, '2019-04-03 04:15:09', '2019-04-03 04:15:09', 'NaN €', 'NaN €', 'NaN €', 1, 'Jun 2025'),
(326, '2019-04-03 04:15:09', '2019-04-03 04:15:09', 'NaN €', 'NaN €', 'NaN €', 1, 'Jul 2025'),
(327, '2019-04-03 04:15:09', '2019-04-03 04:15:09', 'NaN €', 'NaN €', 'NaN €', 1, 'Aug 2025'),
(328, '2019-04-03 04:15:09', '2019-04-03 04:15:09', 'NaN €', 'NaN €', 'NaN €', 1, 'Sep 2025'),
(329, '2019-04-03 04:15:09', '2019-04-03 04:15:09', 'NaN €', 'NaN €', 'NaN €', 1, 'Okt 2025'),
(330, '2019-04-03 04:15:09', '2019-04-03 04:15:09', 'NaN €', 'NaN €', 'NaN €', 1, 'Nov 2025'),
(331, '2019-04-03 04:15:09', '2019-04-03 04:15:09', 'NaN €', 'NaN €', 'NaN €', 1, 'Dez 2025'),
(332, '2019-04-03 04:15:09', '2019-04-03 04:15:09', 'NaN €', 'NaN €', 'NaN €', 1, 'Jan 2026'),
(333, '2019-04-03 04:15:09', '2019-04-03 04:15:09', 'NaN €', 'NaN €', 'NaN €', 1, 'Feb 2026'),
(334, '2019-04-03 04:15:09', '2019-04-03 04:15:09', 'NaN €', 'NaN €', 'NaN €', 1, 'Mär 2026'),
(335, '2019-04-03 04:15:09', '2019-04-03 04:15:09', 'NaN €', 'NaN €', 'NaN €', 1, 'Apr 2026'),
(336, '2019-04-03 04:15:09', '2019-04-03 04:15:09', 'NaN €', 'NaN €', 'NaN €', 1, 'Mai 2026'),
(337, '2019-04-03 04:15:09', '2019-04-03 04:15:09', 'NaN €', 'NaN €', 'NaN €', 1, 'Jun 2026'),
(338, '2019-04-03 04:15:09', '2019-04-03 04:15:09', 'NaN €', 'NaN €', 'NaN €', 1, 'Jul 2026'),
(339, '2019-04-03 04:15:09', '2019-04-03 04:15:09', 'NaN €', 'NaN €', 'NaN €', 1, 'Aug 2026'),
(340, '2019-04-03 04:15:09', '2019-04-03 04:15:09', 'NaN €', 'NaN €', 'NaN €', 1, 'Sep 2026'),
(341, '2019-04-03 04:15:09', '2019-04-03 04:15:09', 'NaN €', 'NaN €', 'NaN €', 1, 'Okt 2026'),
(342, '2019-04-03 04:15:09', '2019-04-03 04:15:09', 'NaN €', 'NaN €', 'NaN €', 1, 'Nov 2026'),
(343, '2019-04-03 04:15:09', '2019-04-03 04:15:09', 'NaN €', 'NaN €', 'NaN €', 1, 'Dez 2026'),
(344, '2019-04-03 04:15:09', '2019-04-03 04:15:09', 'NaN €', 'NaN €', 'NaN €', 1, 'Jan 2027'),
(345, '2019-04-03 04:15:09', '2019-04-03 04:15:09', 'NaN €', 'NaN €', 'NaN €', 1, 'Feb 2027'),
(346, '2019-04-03 04:15:09', '2019-04-03 04:15:09', 'NaN €', 'NaN €', 'NaN €', 1, 'Mär 2027'),
(347, '2019-04-03 04:15:09', '2019-04-03 04:15:09', 'NaN €', 'NaN €', 'NaN €', 1, 'Apr 2027'),
(348, '2019-04-03 04:15:09', '2019-04-03 04:15:09', 'NaN €', 'NaN €', 'NaN €', 1, 'Mai 2027'),
(349, '2019-04-03 04:15:09', '2019-04-03 04:15:09', 'NaN €', 'NaN €', 'NaN €', 1, 'Jun 2027'),
(350, '2019-04-03 04:15:09', '2019-04-03 04:15:09', 'NaN €', 'NaN €', 'NaN €', 1, 'Jul 2027'),
(351, '2019-04-03 04:15:09', '2019-04-03 04:15:09', 'NaN €', 'NaN €', 'NaN €', 1, 'Aug 2027'),
(352, '2019-04-03 04:15:09', '2019-04-03 04:15:09', 'NaN €', 'NaN €', 'NaN €', 1, 'Sep 2027'),
(353, '2019-04-03 04:15:09', '2019-04-03 04:15:09', 'NaN €', 'NaN €', 'NaN €', 1, 'Okt 2027'),
(354, '2019-04-03 04:15:09', '2019-04-03 04:15:09', 'NaN €', 'NaN €', 'NaN €', 1, 'Nov 2027'),
(355, '2019-04-03 04:15:09', '2019-04-03 04:15:09', 'NaN €', 'NaN €', 'NaN €', 1, 'Dez 2027'),
(356, '2019-04-03 04:15:09', '2019-04-03 04:15:09', 'NaN €', 'NaN €', 'NaN €', 1, 'Jan 2028'),
(357, '2019-04-03 04:15:09', '2019-04-03 04:15:09', 'NaN €', 'NaN €', 'NaN €', 1, 'Feb 2028'),
(358, '2019-04-03 04:15:09', '2019-04-03 04:15:09', 'NaN €', 'NaN €', 'NaN €', 1, 'Mär 2028'),
(359, '2019-04-03 04:15:09', '2019-04-03 04:15:09', 'NaN €', 'NaN €', 'NaN €', 1, 'Apr 2028'),
(360, '2019-04-03 04:15:09', '2019-04-03 04:15:09', 'NaN €', 'NaN €', 'NaN €', 1, 'Mai 2028');

-- --------------------------------------------------------

--
-- Table structure for table `tests`
--

CREATE TABLE `tests` (
  `id` int(10) UNSIGNED NOT NULL,
  `hobby` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `kundens_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tests`
--

INSERT INTO `tests` (`id`, `hobby`, `kundens_id`, `created_at`, `updated_at`) VALUES
(1, 'hobby test name', 1, '2018-11-21 16:37:51', '2018-11-21 16:37:51'),
(2, 'hobby test name1', 1, '2018-11-21 16:38:18', '2018-11-21 16:38:18'),
(3, 'hobby test name2_2', 2, '2018-11-21 16:40:12', '2018-11-21 16:40:12');

-- --------------------------------------------------------

--
-- Table structure for table `timeline`
--

CREATE TABLE `timeline` (
  `id` int(10) UNSIGNED NOT NULL,
  `timeline` int(191) DEFAULT NULL,
  `calculation_id` int(11) NOT NULL,
  `kundens_id` int(191) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `finanzierungsbedarf_phase_eins` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `jahreszins_phase_eins` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `laufzeit_phase_eins` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `rate_monatlich_phase_eins` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `restschuld_phase_eins` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `finanzierungsbedarf_phase_zwei` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `jahreszins_phase_zwei` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `laufzeit_phase_zwei` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `rate_monatlich_phase_zwei` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `restschuld_ende` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `timeline`
--

INSERT INTO `timeline` (`id`, `timeline`, `calculation_id`, `kundens_id`, `created_at`, `updated_at`, `finanzierungsbedarf_phase_eins`, `jahreszins_phase_eins`, `laufzeit_phase_eins`, `rate_monatlich_phase_eins`, `restschuld_phase_eins`, `finanzierungsbedarf_phase_zwei`, `jahreszins_phase_zwei`, `laufzeit_phase_zwei`, `rate_monatlich_phase_zwei`, `restschuld_ende`) VALUES
(6, NULL, 1, 1, '2018-11-26 12:06:40', '2018-11-26 12:06:40', '100.000,00', '2', '15', '500', '100.000,00', '100.000,00', '2', '15', '500', '0'),
(7, NULL, 1, 4, '2019-01-17 14:12:38', '2019-01-17 14:12:38', '290000', '2%', '15 Jahre', '290000', '290000', '290000', '2%', '15 Jahre', '290000', '0'),
(8, NULL, 2, 4, '2019-01-17 14:18:11', '2019-01-17 14:18:11', '290000', '2%', '15 Jahre', '290000', '290000', '290000', '2%', '15 Jahre', '290000', '100');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `surname` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mail_address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `status` int(11) DEFAULT '0',
  `group` int(11) DEFAULT '0',
  `admin` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `surname`, `phone`, `mail_address`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`, `status`, `group`, `admin`) VALUES
(5, 'Admin User', 'admin@admin.com', 'admin', '1234', 'homezcasdfsdfas', NULL, '$2y$10$9jdxTHuAZASbg1pc2yePyunFbcBRqnd69jBW1n2guAJ8BHD/jtIcC', NULL, '2019-02-18 13:53:29', '2019-02-25 09:03:49', 1, 1, 1),
(27, 'Patrick', 'p-dierig@einfachmedial.de', 'Dierig', '015204967292', 'p-dierig@einfachmedial.de', NULL, '$2y$10$fOTD1gNVZmPS6GV6WwanXu0ZZI.3IiEPw0bxXrVSjBs3Z6WS4BZ8S', NULL, '2019-02-26 06:38:34', '2019-02-26 06:40:30', 0, 1, 0),
(28, 'Kevin', 'p.dierig@einfachmedial.de', 'Sternad', '0123456789', 'p.dierig@einfachmedial.de', NULL, '$2y$10$/J5g4N7Ao19TSoGsvxlPuebyhkWXjPRBODBWQAIdeixQOpjKPz6/W', NULL, '2019-02-26 06:51:08', '2019-02-26 06:51:08', 0, 2, 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `angebote`
--
ALTER TABLE `angebote`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `calculation`
--
ALTER TABLE `calculation`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `groups`
--
ALTER TABLE `groups`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `immobilies`
--
ALTER TABLE `immobilies`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kundens`
--
ALTER TABLE `kundens`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `repayments`
--
ALTER TABLE `repayments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tests`
--
ALTER TABLE `tests`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `timeline`
--
ALTER TABLE `timeline`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `angebote`
--
ALTER TABLE `angebote`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=352;

--
-- AUTO_INCREMENT for table `calculation`
--
ALTER TABLE `calculation`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `groups`
--
ALTER TABLE `groups`
  MODIFY `id` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `immobilies`
--
ALTER TABLE `immobilies`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `kundens`
--
ALTER TABLE `kundens`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `repayments`
--
ALTER TABLE `repayments`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=361;

--
-- AUTO_INCREMENT for table `tests`
--
ALTER TABLE `tests`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `timeline`
--
ALTER TABLE `timeline`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
